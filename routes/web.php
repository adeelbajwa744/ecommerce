<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/lang/{locale}', 'HomeController@lang');
Route::get('/category/lang/{locale}', 'HomeController@lang');
Route::get('/product/lang/{locale}', 'HomeController@lang');

Route::get('/category/{category}', 'HomeController@ProductByCategory');
Route::get('/product/{product}', 'HomeController@SingleProduct');

Route::get('/create', 'HomeController@create');
Route::get('/delete/{product}', 'HomeController@delete');
Route::get('/edit/{product}', 'HomeController@edit');
Route::post('/save','HomeController@save');
Route::post('/update','HomeController@update');

