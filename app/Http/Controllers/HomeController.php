<?php

// HomeController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\models\Category;
use App\models\Product;
use App\models\Variation;
use App\models\Option;

class HomeController extends Controller
{

    public function lang($locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function index()
    {
        
        $categories = Category::all();
        $products = Product::all();
        return view('welcome',compact('categories','products'));
    }

    public function ProductByCategory($category){
        $categories = Category::all();
        $products = Product::where('category_id',$category)->get();
        return view('welcome',compact('categories','products'));
    }

    public function SingleProduct($product){
        $categories = Category::all();
        $product = Product::find($product);
        return view('product',compact('categories','product'));
    }

    public function create(){
        $products = Product::all();
        $categories = Category::all();
        $variations = Variation::all();
        $options = Option::all();
        return view('create',compact('products','categories','variations','options'));
    }

    public function save(Request $request){
        $product = new Product;
        $product->product_name = $request->pname;
        $product->category_id = $request->category;
        $product->product_img = $request->img;
        $product->product_price = $request->price;
        $product->product_description = $request->description;
        if($product->save()){
            $product->variations()->attach($request->variation);
            $product->options()->attach($request->option);
            return redirect()->back();
        }

    }

    public function delete($product){
        $product = Product::findOrFail($product);
        $product->variations()->detach();
        $product->options()->detach();
        if($product->delete()){
            return redirect()->back();
        }
    }

    public function edit($product){
        $product = Product::find($product);
        $categories = Category::all();
        $variations = Variation::all();
        $options = Option::all();
        return view('edit',compact('product','categories','variations','options'));
    }

    public function update(Request $request){
        $product = Product::find($request->product);
        
        if($product){
            
            $product->product_name = $request->pname;
            $product->category_id = $request->category;
            $product->product_img = $request->img;
            $product->product_price = $request->price;
            $product->product_description = $request->description;
            if($product->save()){
                $product->variations()->sync($request->variation);
                $product->options()->sync($request->option);
                return redirect('/create');
            }
        }
    }
}