<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    public function variationvalues(){
        return $this->hasMany(Variationvalue::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
