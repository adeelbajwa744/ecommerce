<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Variationvalue extends Model
{
    public function variation()
    {
    	return $this->belongsTo(Variation::class);
    }
}
