<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function optionvalues(){
        return $this->hasMany(Optionvalue::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
