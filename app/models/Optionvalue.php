<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Optionvalue extends Model
{
    public function option()
    {
    	return $this->belongsTo(Option::class);
    }
}
