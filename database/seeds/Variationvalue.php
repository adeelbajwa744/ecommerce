<?php

use Illuminate\Database\Seeder;

class Variationvalue extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variationvalues')->insert(
            ['value' => 'red','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('variationvalues')->insert(
            ['value' => 'grey','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('variationvalues')->insert(
            ['value' => 'orange','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );


        DB::table('variationvalues')->insert(
            ['value' => 'large','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('variationvalues')->insert(
            ['value' => 'medium','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('variationvalues')->insert(
            ['value' => 'small','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );
    }
}
