<?php

use Illuminate\Database\Seeder;

class Productvariation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_variation')->insert(
            ['product_id' => '1','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '2','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '3','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );


        DB::table('product_variation')->insert(
            ['product_id' => '4','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '5','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '6','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );



        DB::table('product_variation')->insert(
            ['product_id' => '7','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '8','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '9','variation_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        

        DB::table('product_variation')->insert(
            ['product_id' => '7','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '8','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('product_variation')->insert(
            ['product_id' => '9','variation_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );
    }
}
