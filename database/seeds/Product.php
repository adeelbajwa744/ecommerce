<?php

use Illuminate\Database\Seeder;

class Product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                'product_name' => 'ORIENT GRAND',
                'product_img' => 'https://cdn.shopify.com/s/files/1/2459/1583/products/Grand_2x_c99b3890-176d-4417-970f-a27f1eed8db6_2000x_crop_center.png?v=1585136704',
                'product_price' => '120000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '3',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('products')->insert(
            [
                'product_name' => 'ACSON SPLIT AIR CONDITIONER 2.0 TON',
                'product_img' => 'https://d11zer3aoz69xt.cloudfront.net/media/catalog/product/cache/1/image/1200x/9df78eab33525d08d6e5fb8d27136e95/a/c/acson_split_air_conditioner_16_ton_awm20jalc20c__2.jpg',
                'product_price' => '522000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '3',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('products')->insert(
            [
                'product_name' => 'ALPINA DRY IRON',
                'product_img' => 'https://d11zer3aoz69xt.cloudfront.net/media/catalog/product/cache/1/image/1200x/9df78eab33525d08d6e5fb8d27136e95/a/l/alpina-dry-iron-_sf-2317_.jpg',
                'product_price' => '16000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '3',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );


        DB::table('products')->insert(
            [
                'product_name' => 'Multynet LED TV 55" (NS200)',
                'product_img' => 'https://www.multynet.com.pk/wp-content/uploads/2020/04/LED.jpg',
                'product_price' => '132000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '2',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('products')->insert(
            [
                'product_name' => 'Kenwood 1.5Ton H&C Inverter KES-1830S',
                'product_img' => 'https://cdn.metro-online.pk/dashboard/prod-pic/LHE-01262/12632731-0-S.jpg',
                'product_price' => '105000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '2',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('products')->insert(
            [
                'product_name' => 'Oppo A31 4GB/128GB (White)',
                'product_img' => 'https://www.pakmobizone.pk/wp-content/uploads/2020/03/xoppo-A31-Fantasy-White-200x200.png.pagespeed.ic.XUTeu4uz46.webp',
                'product_price' => '42000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '2',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );



        DB::table('products')->insert(
            [
                'product_name' => 'Heart Print 3/4 Bardot',
                'product_img' => 'https://images-na.ssl-images-amazon.com/images/I/61KzgUXGj%2BL._AC_UX466_.jpg',
                'product_price' => '13000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('products')->insert(
            [
                'product_name' => 'Mix Trousers Long',
                'category_id' => '1',
                'product_img' => 'https://images2.drct2u.com/pdp_main_mobile_x2/products/wf/wf911/n01wf911500s.jpg',
                'product_price' => '11000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('products')->insert(
            [
                'product_name' => 'Match sleep dress',
                'product_img' => 'https://sc01.alicdn.com/kf/UTB88j_WinzIXKJkSafVq6yWgXXac.jpg',
                'product_price' => '11000',
                'product_description'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'category_id' => '1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')]
        );
    }
}
