<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Category::class);
        $this->call(Product::class);
        $this->call(Variation::class);
        $this->call(Variationvalue::class);
        $this->call(Option::class);
        $this->call(Optionvalue::class);
        $this->call(Productvariation::class);
        $this->call(Productoption::class);
    }
}
