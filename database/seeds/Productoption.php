<?php

use Illuminate\Database\Seeder;

class Productoption extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('option_product')->insert(
            ['product_id' => '1','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '2','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '3','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );


        DB::table('option_product')->insert(
            ['product_id' => '1','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '2','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '3','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );


        DB::table('option_product')->insert(
            ['product_id' => '4','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '5','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '6','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );



        DB::table('option_product')->insert(
            ['product_id' => '7','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '8','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('option_product')->insert(
            ['product_id' => '9','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );
    }
}
