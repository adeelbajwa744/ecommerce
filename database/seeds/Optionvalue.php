<?php

use Illuminate\Database\Seeder;

class Optionvalue extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('optionvalues')->insert(
            ['value' => 'default 1','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('optionvalues')->insert(
            ['value' => 'default 2','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('optionvalues')->insert(
            ['value' => 'default 3','option_id' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );


        DB::table('optionvalues')->insert(
            ['value' => 'customized 1','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('optionvalues')->insert(
            ['value' => 'customized 2','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );

        DB::table('optionvalues')->insert(
            ['value' => 'customized 2','option_id' => '2','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]
        );
    }
}
