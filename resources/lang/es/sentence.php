<?php

// sentence.php

return [
  'welcome' => 'Bienvenido amigo',
  'brand' => 'Comprar en linea',
  'category' => 'Categorias',
  'products' => 'Productos',
  'options' => 'Variaciones',
  'variations' => 'Variations',
  'description' => 'Descripción',
];