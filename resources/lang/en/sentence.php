<?php

// sentence.php

return [
  'welcome' => 'Welcome Friend',
  'brand' => 'Buy Online',
  'category' => 'Categories',
  'products' => 'Products',
  'options' => 'Options',
  'variations' => 'Variations',
  'description' => 'Description',
];