<?php

// sentence.php

return [
  'welcome' => 'ようこそ友達',
  'brand' => 'インターネットで買う',
  'category' => '分類',
  'products' => '製品',
  'options' => 'オプション',
  'variations' => 'バリエーション',
  'description' => '解説',
  
];