<?php

// sentence.php

return [
  'welcome' => 'Bienvenue mon ami',
  'brand' => 'Acheter en ligne',
  'category' => 'Catégories',
  'products' => 'Des produits',
  'options' => 'Options',
  'variations' => 'Variations',
  'description' => 'La description',
];
