@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <center><h1> {{ trans('sentence.welcome') }} <h1></center>
            <br /> <br />
        </div>
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item">
                    <h4>{{ trans('sentence.category') }}</h4>
                </li>
                @foreach($categories as $category)
                <li class="list-group-item">
                    <a href="/category/{{ $category->id }}">{{ $category->name }}</a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-9">
            <div class="row">
            @foreach($products as $product)
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <a href="/product/{{ $category->id }}">
                        <img class="card-img-top" src="{{ $product->product_img }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->product_name }}</h5>
                            <p>{{$product->product_price }} Rs</p>

                            <p class="btn btn-sm btn-warning">{{ $product->category->name }}</p>
                        </div>
                        </a>
                        <div class="card-footer">
                            <a href="#" class="btn btn-info">Add Cart</a>
                            <a href="#" class="btn btn-danger">Remove</a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div>
            
        </div>
        
    </div>
@endsection