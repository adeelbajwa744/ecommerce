@extends('layouts.app')

@section('content')
    <div class="row">
    <div class="col-md-12">
            <center><h1> {{ trans('sentence.welcome') }} <h1></center>
            <br /> <br />
        </div>
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item">
                    <h4>{{ trans('sentence.category') }}</h4>
                </li>
                @foreach($categories as $category)
                <li class="list-group-item">
                    <a href="/category/{{ $category->id }}">{{ $category->name }}</a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-9">
        <h4>{{ $product->product_name }}</h4>
        <hr />
            <div class="row">
                <div class="col-md-4">
                    <img style="width:100%;height:300px;" src="{{ $product->product_img }}" />
                </div>

                <div class="col-md-8">
                    <h4>{{ $product->product_price }} Rs</h4>
                    <p><h5>{{ trans('sentence.variations') }}</h5></p>
                    <ul class="list-group">
                    
                        @foreach($product->variations as $variation)
                        <li class="list-group-item">
                            {{ $variation->title }}
                            <p>
                            @foreach($variation->variationvalues as $variationvalue)
                               <input type="radio" name="variation" /> {{ $variationvalue->value }} &nbsp&nbsp&nbsp
                            @endforeach
                            </p>
                        </li>
                        @endforeach
                    </ul>
                    <p><h5>{{ trans('sentence.options') }}</h5></p>
                    <ul class="list-group">
                        @foreach($product->options as $option)
                        <li class="list-group-item">
                            {{ $option->title }}
                            <p>
                            @foreach($option->optionvalues as $optionvalue)
                               <input type="radio" name="option" /> {{ $optionvalue->value }} &nbsp&nbsp&nbsp
                            @endforeach
                            </p>
                        </li>
                        @endforeach
                    </ul>
                    <p>
                        <h4>{{ trans('sentence.description') }}</h4>
                        {{$product->product_description}}
                    </p>
                </div>
            <div>
            
        </div>
        
    </div>
@endsection