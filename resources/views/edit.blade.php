<!DOCTYPE html>
<html>
   <head>
      <title></title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
      <link rel="stylesheet"  href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css" />
   </head>
   <body>
      <div class="card">
         <div class="card-body">
         <form method="POST" action="{{ url('/update') }}">
         @csrf
         <input type="hidden" value="{{ $product->id }}" name="product" class="form-control" required/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>name</lable>
                        <input type="text" value="{{ $product->product_name }}" name="pname" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Category</lable>
                        <select  name="category" class="form-control" required >
                            <option>Select</option>
                            
                            @foreach($categories as $category)
                            <?php 
                                $selected = false;
                                if($category->id === $product->category->id)
                                    $selected=true;
                            ?>
                                <option selected="{{ $selected }}" value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>image</lable>
                        <input type="text" name="img" value="{{ $product->product_img }}" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Price</lable>
                        <input type="number" value="{{ $product->product_price }}" name="price" class="form-control" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Variation</lable>
                        <select  name="variation" multiple="10" class="form-control" required >
                            <?php $distinctvariations = array(); ?>
                            @foreach($product->variations as $variation)
                            <?php $distinctvariations[] = $variation->id ?>
                                <option selected value="{{ $variation->id }}">{{ $variation->title }}</option>
                            @endforeach

                            @foreach($variations as $variation)
                                @if(!in_array($variation->id, $distinctvariations))
                                    <option value="{{ $variation->id }}">{{ $variation->title }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Options</lable>
                        <select  name="option" multiple="10" class="form-control" required >
                            <?php $distinctoptions = array(); ?>
                            @foreach($product->options as $option)
                                <?php $distinctoptions[] = $option->id ?>
                                <option selected value="{{ $option->id }}">{{ $option->title }}</option>
                            @endforeach
                            @foreach($options as $option)
                                @if(!in_array($option->id, $distinctoptions))
                                <option value="{{ $option->id }}">{{ $option->title }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <lable>Description</lable>
                        <textarea name="description" class="form-control" cols="12" rows="5" required>{{ $product->product_description }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </div>
         </form>
         <hr />
         </div>
      </div>
   </body>
</html>