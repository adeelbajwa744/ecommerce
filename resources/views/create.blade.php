<!DOCTYPE html>
<html>
   <head>
      <title></title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
      <link rel="stylesheet"  href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css" />
   </head>
   <body>
      <div class="card">
         <div class="card-body">
         <form method="POST" action="{{ url('/save') }}">
         @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>name</lable>
                        <input type="text" name="pname" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Category</lable>
                        <select  name="category" class="form-control" required >
                            <option>Select</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>image</lable>
                        <input type="text" name="img" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Price</lable>
                        <input type="number" name="price" class="form-control" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Variation</lable>
                        <select  name="variation" multiple="10" class="form-control" required >
                            @foreach($variations as $variation)
                                <option value="{{ $variation->id }}">{{ $variation->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Options</lable>
                        <select  name="option" multiple="10" class="form-control" required >
                            @foreach($options as $option)
                                <option value="{{ $option->id }}">{{ $option->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <lable>Description</lable>
                        <textarea name="description" class="form-control" cols="12" rows="5" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
         </form>
         <hr />
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
               <thead>
                  <tr>
                     <th>Name</th>
                     <th>Category</th>
                     <th>Variations</th>
                     <th>Options</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($products as $product)
                  <tr>
                     <td>{{$product->product_name}}</td>
                     <td>{{$product->category->name}}</td>
                     <td>
                        @foreach($product->variations as $variation)
                        {{$variation->title}} ,
                        @endforeach
                     </td>
                     <td>
                        @foreach($product->options as $option)
                        {{$option->title}} ,
                        @endforeach
                     </td>
                     <td><a href="{{ url( '/edit/'.$product->id ) }}" class="btn btn-info">Edit</a> | <a href="{{ url( '/delete/'.$product->id ) }}" class="btn btn-danger">Delete</a></td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
      <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>
      <script>
         $(document).ready(function() {
             $('#example').DataTable();
         } );
      </script>
   </body>
</html>